package planningservices.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import planningservices.models.Project;
import planningservices.repositories.ProjectRepository;
import planningservices.services.ProjectService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Override
    public Optional<Project> findProjectById(Long id) {
        return projectRepository.findById(id);
    }
    @Override
    public Project saveProject(Project project) {
        projectRepository.save(project);
        System.out.println("Save successfully");
        return project;
    }
    @Override
    public Project updateProject(Project project) {
        projectRepository.save(project);
        System.out.println("Update successfully");
        return project;
    }
    @Override
    public void deleteProject(Long id) {
        projectRepository.deleteById(id);
        System.out.println("Delete successfully");
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }
    @Override
    public int totalProject() {
        return findAll().size();
    }








}
