package planningservices.services;

import planningservices.models.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectService {

    //CRUD
    public Optional<Project> findProjectById(Long id);
    public Project saveProject(Project project);
    public Project updateProject(Project project);
    public void deleteProject(Long id);
    //Other
    public List<Project> findAll();
    public int totalProject();






}
