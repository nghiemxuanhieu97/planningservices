package planningservices.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EffortValidator implements ConstraintValidator<EffortConstraint,Long>{

    @Override
    public void initialize(EffortConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value<=100 && value>=1;
    }


}
