package planningservices;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class PlanningServices {
    public static void main(String[] args)  {
      SpringApplication.run(PlanningServices.class,args);

    }
}

