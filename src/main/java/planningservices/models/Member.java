package planningservices.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name="member")
public class Member {
    @Id
    @GeneratedValue
    @Column(name="memberid")
    Long memberID;
    @Column(name="badgeid",length = 50)
    String badgeID;
    @Column(name="membername",length = 100)
    String name;
    @Column(name="email",length = 100)
    String email;
    @ManyToMany(mappedBy = "members")
    List<Project> projects;
}
