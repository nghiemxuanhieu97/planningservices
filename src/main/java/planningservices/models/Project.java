package planningservices.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import planningservices.validation.EffortConstraint;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Data
@Table(name="project")
@ApiModel(description = "All details about the Project")
public class Project {
    @Id
    @GeneratedValue
    @Column(name="projectid")
    @ApiModelProperty(notes = "The database generated project ID")
    Long projectID;
    @NotEmpty(message = "Please provide Project Name")
    @Size(min=5,message = "Project Name should have at least 5 characters")
    @Column(name = "projectname", length = 100)
    @ApiModelProperty(notes = "The project name")
    String projectName;
//    @NotNull
//    @Max(value = 100,message = "Effort under 100")
    @EffortConstraint(message = "Invalid effort")
    @Column(name = "effort")
    @ApiModelProperty(notes = "The project effort")
    Long effort;
    @Column(name = "startdate")
    @ApiModelProperty(notes = "The project start date")
    Timestamp startDate;
    @Column(name = "updatedate")
    @ApiModelProperty(notes = "The project update date")
    Timestamp updateDate;
    @Column(name="membernum")
    @ApiModelProperty(notes = "The project number of members")
    Long numberOfMembers;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="project_member",
            joinColumns = @JoinColumn(name="projectid"),
            inverseJoinColumns = @JoinColumn(name="memberid"))
    List<Member> members;


}
