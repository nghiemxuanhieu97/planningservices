package planningservices.api;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import planningservices.exception.ResourceNotFoundException;
import planningservices.models.Project;
import planningservices.services.impl.ProjectServiceImpl;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api")
@Api(value = "Project management System", tags = {"Project management System"})
@SwaggerDefinition(tags = {@Tag(name = "Project management System", description = "a")})

public class ProjectAPI {
    @Autowired
    ProjectServiceImpl projectServiceImpl;
    //------------> Not query string in url
    @ApiOperation(value = "Get a project")
    @GetMapping(value="/projects/{projectid}")
    public ResponseEntity<Project> getProject(@PathVariable(name="projectid") Long projectid) throws ResourceNotFoundException{
        Project project = projectServiceImpl.findProjectById(projectid).orElseThrow(()->new ResourceNotFoundException("Project not found with this id : "+projectid));
        return ResponseEntity.ok().body(project);
    }
    //-----------> Create query string in url like ?projectid = 3 or so on
//    @GetMapping(value="/projects")
//    public ResponseEntity<Project> getProject(@RequestParam(value="projectid",required = false,defaultValue = "1") Long projectid)
//    throws ResourceNotFoundException {
//        Project project= projectServiceImpl.findProjectById(projectid)
//                .orElseThrow(()->new ResourceNotFoundException("Project not found with this id : "+projectid));
//        return ResponseEntity.ok().body(project);
//    }
    @ApiOperation(value = "View a list of available projects", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })

    @GetMapping(value="/projects")
    public List<Project> getAllProjects(){
        return projectServiceImpl.findAll();
    }
    @ApiOperation(value = "Add a project")
    @PostMapping(value="/projects")
    public Project createProject(@ApiParam(value = "Project object store in database table", required = true)@Valid @RequestBody Project project){
        return projectServiceImpl.saveProject(project);
    }
    @ApiOperation(value = "Update a project")
    @PutMapping(value = "/projects/{projectid}")
    public ResponseEntity<Project> updateProject(@ApiParam(value = "Project Id to update project object", required = true)@PathVariable(name="projectid")Long projectid,
                                                 @ApiParam(value = "Update project object", required = true)@Valid @RequestBody Project updateProject)
    throws ResourceNotFoundException{
        Project project= projectServiceImpl.findProjectById(projectid)
                .orElseThrow(()->new ResourceNotFoundException("Project not found with this id : "+projectid));
         projectServiceImpl.updateProject(updateProject);
         return ResponseEntity.ok(updateProject);
    }
    @ApiOperation(value = "Delete a project")
    @DeleteMapping(value="/projects/{projectid}")
    public Map<String,Boolean> deleteProject(@ApiParam(value = "Project Id from which project object will delete from database table", required = true)@PathVariable(name="projectid")Long projectid)
    throws ResourceNotFoundException{
        Project project= projectServiceImpl.findProjectById(projectid)
                .orElseThrow(()->new ResourceNotFoundException("Project not found with this id : "+projectid));
        projectServiceImpl.deleteProject(projectid);
        Map<String,Boolean> response = new HashMap<>();
       response.put("Successfully Deleted",Boolean.TRUE);
       return response;
    }


}




