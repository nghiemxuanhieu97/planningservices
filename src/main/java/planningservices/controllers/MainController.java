package planningservices.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import planningservices.services.impl.ProjectServiceImpl;

@Controller
public class MainController {
    @Autowired
    ProjectServiceImpl projectServiceImpl;
    @GetMapping({"/resources","/"})
    public String index(Model model) {
        int totalProject= projectServiceImpl.totalProject();
        model.addAttribute("totalProject",totalProject);
        return "index";
    }


}
