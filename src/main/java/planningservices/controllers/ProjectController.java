package planningservices.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import planningservices.models.Project;
import planningservices.services.impl.ProjectServiceImpl;

@Controller
public class ProjectController {
    @Autowired
    ProjectServiceImpl projectServiceImpl;
    @GetMapping("/listProject")
    public String listProject(Model model){
        model.addAttribute("projectList",projectServiceImpl.findAll());
        return "listProject";
    }
    @GetMapping("/addProject")
    public String addProject(Model model){
        model.addAttribute("project",new Project());
        return "addProject";
    }
    @PostMapping("/addProject")
    public void saveProject(@ModelAttribute Project project){
        projectServiceImpl.saveProject(project);
    }
    @GetMapping("/deleteProject")
    public String deleteProject(){
        return "deleteProject";
    }
    @PostMapping("/deleteProject")
    public void deleteProject(@ModelAttribute(value = "projectID")Long id){
        projectServiceImpl.deleteProject(id);
    }







}
